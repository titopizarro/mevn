const PORT = process.env.PORT || 3000
const express = require('express')
const app = express()

const bodyParser = require('body-parser')
const cors = require('cors')

app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true})) //ojo false

app.use(express.static('public'))

const routes = require('./routes/indexRoutes')(app)

app.listen(PORT, () => {
	console.log(`Server started on port:  ${PORT}`)
})