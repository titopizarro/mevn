const express = require('express')
var router = express.Router()

// add more routes
var tasks = require('./tasksRoutes')

const routes = (app, router) => {

	app.use('/api', tasks)
	
}

module.exports = routes