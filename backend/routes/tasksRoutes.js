const { getAllTasks, addTask, deleteTask, updateTask } = require('../controller/taskController')

const express = require('express')
var router = express.Router()

// GET ALL TASKS
router.get('/tasks', getAllTasks)

// ADD TASK
router.post('/task', addTask)

// UPDATE TASK
router.put('/task/:id', updateTask)

// REMOVE TASK
router.delete('/task/:id', deleteTask)

module.exports = router