const modelTask = require('../model/Task') 

const taskController = {
	
	getAllTasks: (req, res) => {
		modelTask.findAll()
			.then(tasks => {
				res.json(tasks)
			})
			.catch(err => {
				res.json({
					error: err
				})
			})
	},

	addTask: (req, res) => {
		if(!req.body.task_name) {
			res.status(400)
			res.json({
				error: 'Bad data'
			})
		} else {
			modelTask.create(req.body)
				.then(() => {
					res.send('Task Added')
				})
				.catch(err => {
					res.send('Error: ' + err)
				})
		}
	},

	updateTask: (req, res) => {
		if(!req.body.task_name){
			res.status(400)
			res.json({
				error: 'Bad data'
			})
		} else {
			modelTask.update(
				{
					task_name: req.body.task_name
				},
				{
					where: { 
						id: req.params.id 
					}				
				}
			)
				.then(() => {
					res.send('Task updated!!!')
				})
				.catch(err => {
					res.send('error: ' + err)
				})		
		}
	},

	deleteTask: (req, res) => {
		modelTask.destroy({
			where: { 
				id: req.params.id 
			}
		})
			.then(() => {
				res.send('Task deleted!!!')
			})
			.catch(err => {
				res.send('error: ' + err)
			})
	}

}

module.exports = taskController